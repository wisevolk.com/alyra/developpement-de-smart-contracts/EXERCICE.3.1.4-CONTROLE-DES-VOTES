pragma solidity >=0.5.0 <0.6.0;

contract Assemblee {

    address[] private membres;
    Decision[] private decisionsToVote;
    //address[] private aVoter;
    mapping(address => bool) private aVoter;

    struct Decision {
        string description;
        uint votesPour;
        uint votesContre;
    }

    function rejoindre() public {
        membres.push(msg.sender);
    }

    function estMembre(address _utilisateur) public view returns (bool) {
        for(uint i=0; i<membres.length; i++){
            if(membres[i] == _utilisateur){
                return true;
            }
        }
        return false;
    }

    function proposerDecision(string memory _description) public {
        require(estMembre(msg.sender),"Vous devez être membre de l'Assemblee pour voter");
        Decision memory decision = Decision({description:_description,votesPour:0,votesContre:0});
        decisionsToVote.push(decision);
    }

    function _aVoter() private {
        aVoter[msg.sender] = true;
    }

    function voter(uint _indice, bool _vote) public {
        require(estMembre(msg.sender),"Vous devez être membre de l'Assemblee pour voter");
        Decision storage decision = decisionsToVote[_indice];
        require(aVoter[msg.sender] == false, "Vous avez déjà voter !!");
        if(_vote){
            decision.votesPour++;
        } else {
            decision.votesContre++;
        }
        _aVoter();
    }

    function comptabiliser(uint _indice) public view returns (int256) {
        Decision memory decision = decisionsToVote[_indice];

        return int256(decision.votesPour - decision.votesContre);
    }
}
